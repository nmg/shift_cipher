#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "shift_cipher.h"

/**

Nevo Magnezi - UMD Computer Engineering - May 2018

The shift_cipher program uses the shift cipher method of (insecure) 
encryption to encrypt short messages by shifting each lowercase letter by a 
random key. The source for the methods of encryption, answerion, and the 
attack on the encryption method (the "crack" function), is as follows: 
Introduction to Modern Cryptography, Second Edition
by Jonathan Katz & Yehuda Lindelll
ISBN: 978-1466570269

NOTE: This program or any derivatve should not by any means be used for real
life encryption. This is a demonstration project ONLY.

*/

int main(void) {
	int len, key = gen(), answer= 0;
	char message[1000] = {'\0'}; 
	
	printf("I'm a program that encrypts messages by shifting each letter by"
			" a certain amount.\n\nEnter a sufficiently long message to encrypt "
			"(only lowercase letters will be encrypted).\n");

	fgets(message, sizeof(message), stdin);
	len= strlen(message);	

	printf("\nYour key is %d.\nShh! Don't tell anyone!\nI'll throw your"
	" key out. Don't believe me? Check my source file.\n", key);

	key = 0; /*Key gets tossed*/

	sleep(3);
	printf("Type your key in order to encrypt your message.\n");
	scanf("%d", &key);
	
	printf("\nYour message was encrypted as:\n%s\n", enc(message, key));
	sleep(3);

	printf("Select:\n(1) To decrypt your message given your key\n"
			"(2) To allow me to try and crack your message without a "
			"key\n");
	scanf("%d", &answer);
	
	if (answer == 1) {
		printf("\nWhat is your key?\n");
		scanf("%d", &key);
		sleep(1.5);
		printf("\nI decrypted your message to:\n%s\n", dec(message, key));
	}
	
	else if (answer == 2) {
		sleep(1.5);
		printf("\nWithout your key, I decrypted your message to:\n%s\n",
		crack(message, len));
	}
	
	/*Anything else*/
	else { 
		printf("Sorry, I didn't catch that.\n Exiting...");
		exit(-1);
	}
	
	sleep(3);
	printf("\nIn real life, shift ciphers are insecure.\n "
			"Also, it would not be a good encryption scheme to just shift "
			"lowercase letters,"
			"when grammar gives good hints about message content.\n");	
	return 0;	
}


