#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "shift_cipher.h"

/*The gen generates a pseudo-random (NON-secure for real applications!) key 
between 1 and 25, as a shift of 0 would be pretty silly*/
int gen() {
	srand(time(NULL));
	return rand() % 25 + 1;	
}

/*The enc functions encodes a message using the parameter key, by shiffting 
each lowercase letter by the key amount*/
char *enc(char *message, int key) {
	char *c = message;
	/*till end of string*/
	while (*c != '\0') {
		/*only care about lowercase*/
		if (*c >=97 && *c <= 122)
		/*addition of key is circular rather than going through non-letter 
		ASCII values*/
			*c = *c + key > 122 ? *c + key - 26 : *c + key;
		c++;
	}
	return message;
}

/*The dec function decodes a ciphertext into a message by subtracting key from 
each lowercase letter value*/
char *dec(char *ciphertext, int key) {
	char *c = ciphertext;
	/*till end of string*/
	while (*c != '\0') {
    	/*only care about lowercase*/
    	if (*c >=97 && *c <= 122)
    		/*subtraction of key is circular rather than going through non-letter 
    		ASCII values*/
    		*c = *c - key < 97 ? *c - key + 26 : *c - key;
    	c++;	
	}
	return ciphertext;
}

/*The crack function decrypts a ciphertext by comparing the letter frequencies of 
the ciphertext to those that are standard in English. This function works
quite well for even single sentence messages. See chapter 1.3 of the text
described above for greater intuition*/
char *crack(char *ciphertext, int message_len) {
	/*frequencies of letters a-z in english dictionary*/
	float dic_freq[] = {0.08167, 0.01492, 0.02782, 0.04253, 0.12702, 0.02228,
	0.02015, 0.06094, 0.06996, 0.00153, 0.00772, 0.04025, 0.02406, 0.06749,
	0.07507, 0.01929, 0.00095, 0.05987, 0.06327, 0.09056, 0.02758, 0.00978, 
	0.02361, 0.00150, 0.01974, 0.00074}, freq[26]={0},  I_j[26] ={0};
	char *c = ciphertext;
	int key = 0, i, j;
	
	/*determining frequency of letters in the ciphertext*/
	while (*c != '\0') {
		/*only care about lowercase*/
		if (*c >=97 && *c <= 122)
			/*accounting for ACII values in array indexing*/
			freq[(*c) -97]++;
		c++;
	}
	
	/*Using attack on shift cipher described in chapter 1.3 of text described in 
	header*/
	for (j = 0; j < 26; j++) {
		for (i = 0; i < 26; i ++)
			I_j[j]+=freq[(i+j) % 26]/message_len*dic_freq[i];
		I_j[j] -= 0.065539;
		if (I_j[j] < 0) 
			I_j[j] = -I_j[j];
		/*find the sum value that is closest to 0.065 (by using various shifts)*/
		if (I_j[j] <I_j[key])
			key = j;
	}
	/*decrypt using the found key*/
	return dec(ciphertext, key);
}

