CC		= gcc -O3
CFLAGS	= -g -ansi -pedantic-errors -Wall -Wshadow -Wwrite-strings
SRCS	= shift_cipher.c
OBJS	= $(SRCS:.c=.o)

all:		main.x

main.x:		main.o $(OBJS)
			$(CC) $^ -o $@

test%.x:	test%.o $(OBJS) 
			$(CC) $^ -o $@

test%.o:	test%.c 
			$(CC) -c $(CFLAGS) $<

%.o:		%.c %.h
			$(CC) -c $(CFLAGS) $< 

clean:		
			rm -f *.o *.x *.out

